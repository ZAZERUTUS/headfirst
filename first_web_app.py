from flask import Flask, render_template, request, redirect
from search4.search4 import search4letters
from search4.log_search4_page import Log_1
from test1.test1 import Test1, Log_test1

app = Flask(__name__)

@app.route('/home', methods=['GET'])
def home_page() -> 'html':
    return render_template('HomePage.html', the_title="Welcome!!!")
    
@app.route('/search4', methods=['POST','GET'])
def search4_page() -> 'html':
    return render_template('search4/Search4Page.html', the_title="Welcome in search4!!!")
    
@app.route('/')
def hello() -> '302':
    return redirect('/home')
    
@app.route('/result', methods=['POST', 'GET'])
def search4() -> 'html':
    phrase = request.form['phrase']
    letters = request.form['letters']
    ret_def = ' '.join(search4letters(phrase, letters))
    title = 'Result:'
    arr = [phrase, letters, ret_def]
    Log_1.log_request(arr)
    return render_template('search4/ResultPage.html', the_phrase=phrase, the_letters=letters, the_result=ret_def, the_title=title)
    
@app.route('/logsearch4')
def view_log_search4() -> 'html':
    fl = Log_1.log_read()
    hd = ['Phrase', 'Letters', 'Result']
    return render_template('log/LogPageTM.html', the_log=fl, the_headers=hd)
    
@app.route('/logtest1')
def view_log_test1() -> 'html':
    fl = Log_test1.log_read()
    hd = ['Name', 'Result']
    return render_template('log/LogPageTM.html', the_log=fl, the_headers=hd)
    
@app.route('/loghome')
def view_log_home() -> 'html':
    return render_template('log/LogPageHome.html')
    
@app.route('/test1', methods=['POST', 'GET'])
def test1_page() -> 'html':
    return render_template('test1/Test1Page.html', the_title="Welcome in test1!!!")
    
@app.route('/test1result', methods=['POST', 'GET'])
def test1result_page() -> 'html':
    name = request.form['name']
    tst = Test1()
    res = tst.test1(name)
    Log_test1.log_request([name, res])
    return render_template('test1/Test1ResultPage.html', the_result=res)
    
if __name__ == '__main__':
    app.run(debug=True)

