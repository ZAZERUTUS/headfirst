DELIMETR = '-/@/-'


class Log_1:

    def log_request(qvar_req) -> None:
        with open('log/vsearch.log', 'a') as fl:
            sd = f'{qvar_req[0]}{DELIMETR}{qvar_req[1]}{DELIMETR}{qvar_req[2]}'
            print(sd, file=fl)
            
    def log_read():
        with open('log/vsearch.log', 'r') as fl:
            res = list()
            for a in fl:
                res.append(a.split(DELIMETR))
            return res
