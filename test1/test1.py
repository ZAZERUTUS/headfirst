DELIMETR = '-/@/-'

class Test1:
    def __init__(self):
        self.LST = ['наташа', 'наталья', 'натали', 'natali', 'natasha']

    def test1(self, name: str):
        bl = [True for i in self.LST if name.lower() == i] 
        if True in bl:
            return f'{name} - Пиндос'
        return f'{name} - Не пиндос'
        
        
class Log_test1:

    def log_request(qvar_req) -> None:
        with open('log/test1.log', 'a') as fl:
            sd = f'{qvar_req[0]}{DELIMETR}{qvar_req[1]}'
            print(sd, file=fl)
            
    def log_read():
        with open('log/test1.log', 'r') as fl:
            res = list()
            for a in fl:
                res.append(a.split(DELIMETR))
            return res
